<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
   <link href="{{asset('css/custom.css')}}" rel="stylesheet">
   <title>Bladetemplate</title>

</head>
<body>   
   @auth
   @else
   <div class="container-fluid h-100 d-inline-block main-container jubotron main-bg">
      <div class="row">
         <div class="container-fluid my-5 col-12">
         
         </div>
   
         <div class="col-3 col-md-4"></div>

         <div class="container my-5 col-6 col-md-4 border border-light bg-light">
            <div class="p-3">
               
               <form action="/register" method="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                  
                  <div class="mb-4">
                     <label class="p-2">name</label>
                     <input class="form-control" type="text" placeholder="enter user name" name="name">
                  </div>

                  <div class="mb-4">
                     <label class="p-2">mail</label>
                     <input class="form-control" type="text" placeholder="enter your mail adress" name="email">
                  </div>
                  
                  <div class="mb-4">
                     <label class="p-2">Password</label>
                     <input class="form-control" type="password" placeholder="enter your password" name="password">
                  </div>
                                   
                  <button class="btn btn-dark p-2 mt-2 col-12">Register</button>                
               </form>

              
               

            </div>
         </div>

         <div class="col-3 col-md-4"></div>

      </div>


   </div>







   <script src="{{asset('js/bootstrap.js')}}"></script>
   @endauth
</body>
</html>