<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
   <link href="{{asset('css/custom.css')}}" rel="stylesheet">
   <title>Bladetemplate</title>
</head>
<body>
   @auth
   <div class="container-fluid h-100 d-inline-block main-container jubotron home-bg">
      
      <nav class="navbar navbar-expand-lg border-bottom border-dark">
         <div class="container-fluid">
            
            <div class="col-2">
               <h2>Linketeer</h2>
            </div>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown">
               <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse col-8" id="navbarNavDropdown">
               <div class="col-lg-2 d-flex justify-content-center p-1">
                  <button class="btn btn-light">                  
                  <a href="{{url('home')}}" class="link-dark link-underline-opacity-0">Home</a>
                  </button>                  
               </div>               
               <div class="col-lg-2 d-flex justify-content-center p-1">  
                  <button class="btn btn-light">                
                  <a href="{{url('createlink')}}" class="link-dark link-underline-opacity-0">Add link</a> 
                  </button>                 
               </div>
               <div class="col-lg-2 d-flex justify-content-center p-1">                  
                  <button class="btn btn-light">
                  <a href="{{url('createcategory')}}" class="link-dark link-underline-opacity-0">Add category</a>
                  </button>                  
               </div>
               <div class="col-lg-2 d-flex justify-content-center p-1">                  
                  <button class="btn btn-light">
                  <a href="{{url('homecategory')}}" class="link-dark link-underline-opacity-0">Edit categories</a>
                  </button>                  
               </div>               
               <div class="col-lg-2 d-flex justify-content-center p-1">
                  <button class="btn btn-light">                  
                  <a href="{{url('createsubcategory')}}" class="link-dark link-underline-opacity-0">Add subcategory</a>
                  </button>                  
               </div>
               <div class="col-lg-2 d-flex justify-content-center p-1 ms-2">
                  <button class="btn btn-light">                  
                  <a href="{{url('homesubcategory')}}" class="link-dark link-underline-opacity-0">Edit subcategories</a>
                  </button>                  
               </div>               
            </div> 
            <div class="collapse navbar-collapse col-2" id="navbarNavDropdown">
               <div class="col-lg-12 d-flex justify-content-lg-end justify-content-center ms-2">
                  <form action="/logout" method="POST" class="mt-3 me-3">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
                     <button class="btn btn-light">Log out</button>
                  </form> 
               </div>
            </div>        
         </div>
     </nav> 
      
      <div class="row">
         <div class="container-fluid my-5 col-12">
         </div>
   
         <div class="col-3 col-md-4"></div>

         <div class="container my-5 col-6 col-md-4 border border-light bg-light">
            <div class="p-3">
               <form action="/editlink/{{$post->id}}" method="POST"> 
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                  @method('PUT')

                  <div class="mb-4">
                     <label class="p-2">Title</label>
                     <input class="form-control" type="text"  name="title" value="{{$post->title}}">
                  </div>

                  <div class="mb-4">
                     <label class="p-2">Link</label>
                     <input class="form-control" type="text" name="link" value="{{$post->link}}">
                  </div>
                  <div>
                     <label class="p-2">Category</label>
                     <select id="categorySelect" name="category" class="form-select">  
                         <option value="{{$post->category}}" selected>{{$post->category}}</option>
                         @foreach($categories as $category)
                             @if($category['name'] !== $post->category)
                                 <option value="{{$category['name']}}">{{$category['name']}}</option>
                             @endif
                         @endforeach
                     </select>
                 </div>
                 
                 <div>
                     <label class="p-2">Subcategory</label>
                     <select id="subcategorySelect" name="subcategory" class="form-select">                    
                         <option value="default" selected>default</option>
                         <!-- Options will be dynamically populated using JavaScript -->
                     </select>
                 </div>
                 
                 <script>
                     document.addEventListener('DOMContentLoaded', function () {
                         const categorySelect = document.getElementById('categorySelect');
                         const subcategorySelect = document.getElementById('subcategorySelect');
                 
                         updateSubcategories();
                         categorySelect.addEventListener('change', function () {
                             updateSubcategories();
                         });
                 
                         function updateSubcategories() {
                             const selectedCategory = categorySelect.value;
                 
                             subcategorySelect.innerHTML = '<option value="default" selected>default</option>';
                 
                             @foreach($categories as $category)
                                 if ('{{$category['name']}}' === selectedCategory) {
                                     @foreach ($category->categorySubcategories as $subcategory)
                                         subcategorySelect.innerHTML += `<option value="{{$subcategory['name']}}">{{$subcategory['name']}}</option>`;
                                     @endforeach
                 
                                     // If no subcategories, set the default option
                                     if ('{{count($category->categorySubcategories)}}' === '0') {
                                         subcategorySelect.value = 'default';
                                     }
                                 }
                             @endforeach
                         }
                     });
                 </script>



                  <button class="btn btn-dark p-2 mt-2 col-12">Confirm</button>
               </form>        
               
            </div>
         </div>

         <div class="col-3 col-md-4"></div>

      </div>
   </div>



   <script src="{{asset('js/bootstrap.js')}}"></script>
   @else
   <div class="container-fluid h-100 d-inline-block main-container jubotron main-bg">
      <br><br> 
      <div class="row">
         <div class="col-4"></div>
         <div class="bg-light border border-danger col-4">
            <div class="container d-flex justify-content-center mt-5">      
               <H2 class="text-danger text-center">No user is logged in</H2>     
            </div>
            <br> 
            <div class="container d-flex justify-content-center p-2 mb-5">
               <button class="btn btn-danger text-center justify-content-center">
                  <a href="{{url('')}}" class="link-dark link-underline-opacity-0 text-light">Return to the login screen</a>
               </button>
            </div>
         </div>
         <div class="col-4"></div>
      </div>
   </div>
   @endauth
</body>
</html>