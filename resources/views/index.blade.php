<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
   <link href="{{asset('css/custom.css')}}" rel="stylesheet">
   <title>login</title>
</head>
<body> 
   <div class="bg-light fixed-top mb-5"><h2 class="text-center p-2">Linketeer - browser app for sorting your links</h2>
   </div>    
   <div class="container-fluid h-100 d-inline-block main-container jubotron main-bg">
       
      <div class="row my-5">
         <div class="container-fluid mt-5 col-12">
               
         </div>
   
         <div class="col-3 col-md-4"></div>

         <div class="container my-5 col-6 col-md-4 border border-light bg-light">
            <div class="p-3">
               <form action="/login" method="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                  
                  <div class="mb-3">
                     <label class="p-2">Username</label>
                     <input class="form-control" type="text" placeholder="username" name="loginname">
                  </div>
                  
                  <div class="mb-3">
                     <label class="p-2">Password</label>
                     <input class="form-control" type="password" placeholder="password" name="loginpassword">
                  </div>
                  
                  @if(count($errors))
                  <div class="alert alert-danger">
                     <ul>
                        @foreach($errors->all() as $error)                     
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                  </div>
                  @endif

                  <p class="text-end"></p>

                  <button class="btn btn-dark text-light p-2 mt-2 col-12">Log in</button>
                  
               </form>

               <p class="text-center">or</p>

               <div class="d-flex justify-content-center">
                  <form action="/register" method="GET">
                     <Button class="btn btn-dark text-light"> <a href="{{url('register')}}" class="link-light link-underline-opacity-0"> Sign Up </a></Button>
                  </form>
               </div>
               

            </div>
         </div>

         <div class="col-3 col-md-4"></div>

      </div>
      
   </div>
  
   <script src="{{asset('js/script.js')}}"></script>
   <script src="{{asset('js/bootstrap.js')}}"></script>
</body>
</html>