<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
   <link href="{{asset('css/datatables.css')}}" rel="stylesheet">
   <link href="{{asset('css/custom.css')}}" rel="stylesheet">
   <title>Home</title>
</head>
<body>
   @auth
   <div class="container-fluid h-100 d-inline-block main-container jubotron home-bg">
      
      <nav class="navbar navbar-expand-lg">
         <div class="container-fluid bg-light border-bottom border-dark">
            
            <div class="col-2">
               <h2>Linketeer</h2>               
            </div>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown">
               <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse col-8" id="navbarNavDropdown">
               <div class="col-lg-2 d-flex justify-content-center p-1">
                  <button class="btn btn-light">                  
                  <a href="{{url('home')}}" class="link-dark link-underline-opacity-0">Home</a>
                  </button>                  
               </div>               
               <div class="col-lg-2 d-flex justify-content-center p-1">  
                  <button class="btn btn-light">                
                  <a href="{{url('createlink')}}" class="link-dark link-underline-opacity-0">Add link</a> 
                  </button>                 
               </div>
               <div class="col-lg-2 d-flex justify-content-center p-1">                  
                  <button class="btn btn-light">
                  <a href="{{url('createcategory')}}" class="link-dark link-underline-opacity-0">Add category</a>
                  </button>                  
               </div>
               <div class="col-lg-2 d-flex justify-content-center p-1">                  
                  <button class="btn btn-light">
                  <a href="{{url('homecategory')}}" class="link-dark link-underline-opacity-0">Edit categories</a>
                  </button>                  
               </div>               
               <div class="col-lg-2 d-flex justify-content-center p-1">
                  <button class="btn btn-light">                  
                  <a href="{{url('createsubcategory')}}" class="link-dark link-underline-opacity-0">Add subcategory</a>
                  </button>                  
               </div>
               <div class="col-lg-2 d-flex justify-content-center p-1 ms-2">
                  <button class="btn btn-light">                  
                  <a href="{{url('homesubcategory')}}" class="link-dark link-underline-opacity-0">Edit subcategories</a>
                  </button>                  
               </div>               
            </div> 
            <div class="collapse navbar-collapse col-2" id="navbarNavDropdown">
               <div class="col-lg-12 d-flex justify-content-lg-end justify-content-center ms-2">
                  <form action="/logout" method="POST" class="mt-3 me-3">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
                     <button class="btn btn-light">Log out</button>
                  </form> 
               </div>
            </div>        
         </div>
     </nav> 

      <div class="row"> 
         @if (!session('alert_shown'))
            <div id="myAlert" class="alert alert-primary alert-dismissible fade show" role="alert">
               <p class="h3 mb-2 p-2 text-center"> 
                  Tato webová aplikace byla vytvořena jako součást maturitní práce. Ve spodní části domovské stránky je odkaz na dotazník, týkající se této aplikace. Byl bych moc vděčný za Vaši zpětnou vazbu. 
                  &lpar;Aplikace bude dostupná pouze od 8.2.2024 do 8.3.2024&rpar;        
               </p>
               <p class="text-muted h3 p-2 text-center">
                  This web application was created as part of the Maturita thesis. At the bottom of the page, there is a link to a questionnaire related to this application. I would be very grateful for your feedback. &lpar;The app will only be available from 8.2.2024 to 8.3.2024&rpar;
               </p>
      
               <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
               </button>
            </div>     
            <script>         
               $(document).ready(function () {
                  var myAlert = $('#myAlert');                 
                  setTimeout(function () {
                        myAlert.fadeOut(1000);
                  }, 30000);
                  <?php session(['alert_shown' => true]); ?>
               });
            </script>
         @endif


      </div>

      <div class="row mt-5">
         <div class="col-1"></div>
         <div class="container-fluid col-10">
            <table id="links" class="table table-striped" style="width:100%">               
               <thead>
                  <tr>
                     <th>Title</th>                     
                     <th>Link</th>
                     <th>Category</th>
                     <th>Subcategory</th>
                     <th>Time aded</th>
                     <th>Edit</th>
                     <th>Delete</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach($posts as $post)
                  <tr>
                     <td>{{$post['title']}}</td>
                     <td><a href="{{$post['link']}}" target="_blank">{{$post['link']}}</a></td>
                     <td>{{$post['category']}}</td>
                     <td>{{$post['subcategory']}}</td>
                     <td>{{$post['created_at']}}</td>
                     <td><a href="/editlink/{{$post->id}}"><button class="btn btn-primary">Edit</button></a></td>
                     <td>
                        <form action="/delete-post/{{$post->id}}" method="POST">                           
                           <input type="hidden" name="_token" value="{{ csrf_token() }}"/>                        
                           @method('DELETE')                                                
                           <button class="btn btn-danger">Delete</button>
                        </form>  </td>
                  </tr>
                  
                  @endforeach
               </tbody>
                  
           </table>
         </div>
         <div class="col-1"></div>
      </div>
   
   
   <footer class="footer bg-light justify-content-center fixed-bottom d-flex mt-5 border-top border-dark">     
     <button class="btn btn-danger p-2 my-2">
      <a class="link-light link-underline-opacity-0" target="_blank" href="https://docs.google.com/forms/d/1OR6FMvxCPvcKwjr3-BxhzGYtxi7PPvt-IN8ftAGicwU/edit"> Feedback form </a>
      </button>      
   </footer>
      




   <script src="{{asset('js/jquery-3.6.0.js')}}"></script>
   <script src="{{asset('js/datatables.js')}}"></script>
   <script src="{{asset('js/script.js')}}"></script>
   <script src="{{asset('js/bootstrap.js')}}"></script>
   @else
   <div class="container-fluid h-100 d-inline-block main-container jubotron main-bg">
      <br><br> 
      <div class="row">
         <div class="col-4"></div>
         <div class="bg-light border border-danger col-4">
            <div class="container d-flex justify-content-center mt-5">      
               <H2 class="text-danger text-center">No user is logged in</H2>     
            </div>
            <br> 
            <div class="container d-flex justify-content-center p-2 mb-5">
               <button class="btn btn-danger text-center justify-content-center">
                  <a href="{{url('')}}" class="link-dark link-underline-opacity-0 text-light">Return to the login screen</a>
               </button>
            </div>
         </div>
         <div class="col-4"></div>
      </div>
   </div>
   @endauth
</body>
</html>