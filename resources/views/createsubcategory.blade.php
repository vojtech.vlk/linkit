<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
   <link href="{{asset('css/custom.css')}}" rel="stylesheet">
   <title>Bladetemplate</title>
</head>
<body>
   @auth
   <div class="container-fluid h-100 d-inline-block main-container jubotron home-bg">
      
      <nav class="navbar navbar-expand-lg border-bottom border-dark">
         <div class="container-fluid">
            
            <div class="col-2">
               <h2>Linketeer</h2>
            </div>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown">
               <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse col-8" id="navbarNavDropdown">
               <div class="col-lg-2 d-flex justify-content-center p-1">
                  <button class="btn btn-light">                  
                  <a href="{{url('home')}}" class="link-dark link-underline-opacity-0">Home</a>
                  </button>                  
               </div>               
               <div class="col-lg-2 d-flex justify-content-center p-1">  
                  <button class="btn btn-light">                
                  <a href="{{url('createlink')}}" class="link-dark link-underline-opacity-0">Add link</a> 
                  </button>                 
               </div>
               <div class="col-lg-2 d-flex justify-content-center p-1">                  
                  <button class="btn btn-light">
                  <a href="{{url('createcategory')}}" class="link-dark link-underline-opacity-0">Add category</a>
                  </button>                  
               </div>
               <div class="col-lg-2 d-flex justify-content-center p-1">                  
                  <button class="btn btn-light">
                  <a href="{{url('homecategory')}}" class="link-dark link-underline-opacity-0">Edit categories</a>
                  </button>                  
               </div>               
               <div class="col-lg-2 d-flex justify-content-center p-1">
                  <button class="btn btn-light">                  
                  <a href="{{url('createsubcategory')}}" class="link-dark link-underline-opacity-0">Add subcategory</a>
                  </button>                  
               </div>
               <div class="col-lg-2 d-flex justify-content-center p-1 ms-2">
                  <button class="btn btn-light">                  
                  <a href="{{url('homesubcategory')}}" class="link-dark link-underline-opacity-0">Edit subcategories</a>
                  </button>                  
               </div>               
            </div> 
            <div class="collapse navbar-collapse col-2" id="navbarNavDropdown">
               <div class="col-lg-12 d-flex justify-content-lg-end justify-content-center ms-2">
                  <form action="/logout" method="POST" class="mt-3 me-3">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
                     <button class="btn btn-light">Log out</button>
                  </form> 
               </div>
            </div>        
         </div>
     </nav> 
      
      <div class="row">
         <div class="container-fluid my-5 col-12">
         </div>
   
         <div class="col-3 col-md-4"></div>

         <div class="container my-5 col-6 col-md-4 border border-light bg-light">
            <div class="p-3">
               <form action="/create-subcategory" method="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                  
                  <div class="mb-4">
                     <div class="mb-4">
                        <label class="p-2">Name</label>
                        <input class="form-control" type="text"  name="name">
                     </div> 
                     <div class="mb-4">
                        <label class="p-2">Subcategory of</label>
                        <select name="category_id" class="form-select">                
                           @foreach($categories as $category)
                           <option value="{{$category['id']}}">{{$category['name']}}</option>
                           @endforeach
                        </select>
                     </div>                     
                  </div>
                           
                  <button class="btn btn-dark p-2 mt-2 col-12">Confirm</button>
               </form>        
               
            </div>
         </div>

         <div class="col-3 col-md-4"></div>

      </div>
   </div>



   <script src="{{asset('js/bootstrap.js')}}"></script>
   @else
   <div class="container-fluid h-100 d-inline-block main-container jubotron main-bg">
      <br><br> 
      <div class="row">
         <div class="col-4"></div>
         <div class="bg-light border border-danger col-4">
            <div class="container d-flex justify-content-center mt-5">      
               <H2 class="text-danger text-center">No user is logged in</H2>     
            </div>
            <br> 
            <div class="container d-flex justify-content-center p-2 mb-5">
               <button class="btn btn-danger text-center justify-content-center">
                  <a href="{{url('')}}" class="link-dark link-underline-opacity-0 text-light">Return to the login screen</a>
               </button>
            </div>
         </div>
         <div class="col-4"></div>
      </div>
   </div>
   @endauth
</body>
</html>