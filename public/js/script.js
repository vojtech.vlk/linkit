$(document).ready(function () {
   $('#links').DataTable({
      bLengthChange: false,
      pageLength: 8,      
      bInfo: false,
      
      dom: 'fBrtip',
      buttons: [
         {extend: 'pdf', 
         className: 'btn btn-light mb-3',
         text: 'Export table as PDF'
      }
      ],

      columnDefs: [
         {"className": "dt-center", "targets": "_all"},
       ]
   });
});

$(document).ready(function () {
   $('#categories').DataTable({
      bLengthChange: false,
      pageLength: 8,      
      bInfo: false,
            

      columnDefs: [
         {"className": "dt-center", "targets": "_all"},
       ]
   });
});



