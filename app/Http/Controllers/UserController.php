<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function register(Request $request){
        $incomingFields = $request->validate([
            'name' => ['required', 'min:3', 'max:10', Rule::unique('users', 'name')],
            'email' => ['required', 'email', Rule::unique('users', 'email')],
            'password' => ['required', 'min:8', 'max:200']
        ]);

        $incomingFields ['password'] = bcrypt($incomingFields ['password']);
        $user = User::create($incomingFields);
        auth()->login($user);

        return redirect('/home');
        }

    public function logout(Request $request){
        $request->session()->forget('alert_shown');
        auth()->logout();
        return redirect('/');
        }
        
        
    public function login(Request $request){
        $incomingFields = $request->validate([
            'loginname' => 'required',
            'loginpassword' => 'required' 
        ]);

        if(auth()->attempt(['name'  =>  $incomingFields['loginname'], 'password' => $incomingFields['loginpassword']])){
            $request->session()->regenerate();
            return redirect('/home');
        }

        return back()->withInput()->withErrors(['email' => 'Entered username or password is invalid']);

        }

        
}
