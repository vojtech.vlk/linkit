<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function showEditCategory(Category $category){
        if(auth()->user()->id !== $category['user_id']){
            return redirect('/');  
        }

        return view('editcategory', ['category' => $category]);
    }

    public function createCategory(Request $request) {
        $incomingFields = $request->validate([
            'name' => 'required'
        ]);
        
        $incomingFields ['name'] = ($incomingFields['name']);
        $incomingFields ['user_id'] = auth()->id();
        Category::create($incomingFields);
        return redirect('/homecategory');
    }

    public function actuallyUpdateCategory(Category $category, Request $request){
        if(auth()->user()->id !== $category['user_id']){
            return redirect('/');
        }

        $incomingFields = $request->validate([
            'name' => 'required'
        ]); 

        $incomingFields ['name'] = ($incomingFields['name']);

        $category->update($incomingFields);
        return redirect('/homecategory');
    }

    public function deleteCategory(Category $category){
        if(auth()->user()->id == $category['user_id']){
            $category->delete();
        }

        return redirect('/homecategory');
    }
}
