<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
    public function showEditSubcategory(Subcategory $subcategory){
        $categories = [];
        if (auth()->check()) {
        $categories = auth()->user()->usersCategories()->latest()->get();
        }
        return view('editsubcategory', ['subcategory' => $subcategory, 'categories'=> $categories]);
    }

    public function createSubcategory(Request $request) {
        $incomingFields = $request->validate([
            'name' => 'required',
            'category_id' => 'required'  
        ]);
        
        $incomingFields ['name'] = ($incomingFields['name']);
        $incomingFields ['category_id'] = ($incomingFields['category_id']);

        Subcategory::create($incomingFields);
        return redirect('/homesubcategory');
    }

    public function actuallyUpdateSubcategory(Subcategory $subcategory, Request $request){
        
        $incomingFields = $request->validate([
            'name' => 'required',
            'category_id' => 'required'     
        ]);

        $incomingFields ['name'] = ($incomingFields['name']);
        $incomingFields ['category_id'] = ($incomingFields['category_id']);
        $subcategory->update($incomingFields);
        return redirect('/homesubcategory');
    }

    public function deleteSubcategory(Subcategory $subcategory){
        
            $subcategory->delete();
        

        return redirect('/homesubcategory');
    }
}
