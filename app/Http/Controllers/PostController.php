<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function showEditScreen(Post $post){
        if(auth()->user()->id !== $post['user_id']){
            return redirect('/');
        }
        $categories = [];
        $subcategories = [];   
        $categories = auth()->user()->usersCategories()->latest()->get();
        $subcategories = auth()->user()->usersCategories()->with('categorySubcategories')->latest()->get();   
        return view('editlink', ['post' => $post, 'categories'=> $categories, 'subcategories' => $subcategories]);
    }

    public function createPost(Request $request) {
        $incomingFields = $request->validate([
            'title' => 'required', 
            'link' => 'required',
            'category' => 'required',
            'subcategory' => 'required'
        ]);
        
        $incomingFields ['title'] = strip_tags($incomingFields['title']);
        $incomingFields ['link'] = ($incomingFields['link']);
        $incomingFields ['category'] = ($incomingFields['category']);
        $incomingFields ['subcategory'] = ($incomingFields['subcategory']);
        $incomingFields ['user_id'] = auth()->id();
        Post::create($incomingFields);
        return redirect('/home');
    }

    public function actuallyUpdatePost(Post $post, Request $request){
        if(auth()->user()->id !== $post['user_id']){
            return redirect('/');
        }
        

        $incomingFields = $request->validate([
            'title' => 'required', 
            'link' => 'required',
            'category' => 'required',
            'subcategory' => 'required'
        ]);

        $incomingFields ['title'] = strip_tags($incomingFields['title']);
        $incomingFields ['link'] = ($incomingFields['link']);
        $incomingFields ['category'] = ($incomingFields['category']);
        $incomingFields ['subcategory'] = ($incomingFields['subcategory']);
        $post->update($incomingFields);
        return redirect('/home');
    }

    public function deletePost(Post $post){
        if(auth()->user()->id == $post['user_id']){
            $post->delete();
        }

        return redirect('/home');
    }
}
