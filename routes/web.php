<?php
use App\Models\Category;
use App\Models\Post;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubcategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
//routy na přesměrování
Route::get('/', function () {
    return view('index');
});
Route::get('/register', function () {
    return view('register');
});
Route::get('/home', function () {
    $posts = [];
    if (auth()->check()) {
        $posts = auth()->user()->usersPosts()->latest()->get();
    }
    return view('home', ['posts'=> $posts]);
});
Route::get('/homecategory', function () {
    $categories = [];
    if (auth()->check()) {
        $categories = auth()->user()->usersCategories()->latest()->get();
    }
    return view('homecategory', ['categories'=> $categories]);
});
Route::get('/createlink', function () {
    $categories = [];
    $subcategories = [];
    if (auth()->check()) {
        $categories = auth()->user()->usersCategories()->latest()->get();
        $subcategories = auth()->user()->usersCategories()->with('categorySubcategories')->latest()->get() ;
    }
    return view('createlink', ['categories'=> $categories, 'subcategories' => $subcategories]);
});
Route::get('/editlink', function () {
    
    return view('editlink');    
});

Route::get('/createcategory', function () {
    return view('createcategory');
});


Route::get('/createsubcategory', function () {
    $categories = [];
    if (auth()->check()) {
        $categories = auth()->user()->usersCategories()->latest()->get();
    }
    return view('createsubcategory', ['categories'=> $categories]);
});

Route::get('/homesubcategory', function () {
    $categories = [];
    $subcategories = [];
    if (auth()->check()) {
        $categories = auth()->user()->usersCategories()->latest()->get();
        $subcategories = auth()->user()->usersCategories()->with('categorySubcategories')->latest()->get() ;
    }
    return view('homesubcategory', ['categories'=> $categories, 'subcategories' => $subcategories]);
});



//registrace, přihlašování a odhlašování
Route::post('/register', [UserController::class, 'register']);
Route::post('/logout', [UserController::class, 'logout']);
Route::post('/login', [UserController::class, 'login']);

//vytvoření postu, editace, mazání
Route::post('/create-post', [PostController::class, 'createPost']);
Route::get('/editlink/{post}', [PostController::class, 'showEditScreen']);
Route::put('/editlink/{post}', [PostController::class, 'actuallyUpdatePost']);
Route::delete('/delete-post/{post}', [PostController::class, 'deletePost']);

//vytvoření, editace a mazání kategorií
Route::post('/create-category', [CategoryController::class, 'createCategory']);
Route::get('/editcategory/{category}', [CategoryController::class, 'showEditCategory']);
Route::put('/editcategory/{category}', [CategoryController::class, 'actuallyUpdateCategory']);
Route::delete('/delete-category/{category}', [CategoryController::class, 'deleteCategory']);

Route::post('/create-subcategory', [SubcategoryController::class, 'createSubcategory']);
Route::get('/editsubcategory/{subcategory}', [SubcategoryController::class, 'showEditSubcategory']);
Route::put('/editsubcategory/{subcategory}', [SubcategoryController::class, 'actuallyUpdateSubcategory']);
Route::delete('/delete-subcategory/{subcategory}', [SubcategoryController::class, 'deleteSubcategory']);

